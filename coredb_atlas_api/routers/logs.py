
# External
from flask import make_response, abort

# Internal
from config import mongo

def read_all():
    # That would go poorly... But maybe I'll think of a way to implement it
    pass

def read_subject_logs(subject_id):

    # Fetch the data
    pipeline = [
        {
            "$match": {"subject_id": subject_id}
        },
        {
            "$lookup": {
                "from": "CombinedLogs",
                "localField": "log_id",
                "foreignField": "log_id",
                "as": "CombinedLogs"
            }
        },
        {
            "$unwind": "$CombinedLogs"
        },
        {
            "$limit": 100
        },
        {
            "$project": {
                "_id": 0,
                "CombinedLogs._id": 0
            }
        }
    ]

    logs = [doc for doc in mongo.db.Metadata.aggregate(pipeline)]

    return logs