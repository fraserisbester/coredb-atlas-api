import os
import connexion
from flask_pymongo import PyMongo

# Dev
from getpass import getpass

basedir = os.path.abspath(os.path.dirname(__file__))

# Create the connexion application instance
connex_app = connexion.App(__name__, specification_dir=basedir)

# Get the underlying Flask app instance
app = connex_app.app

# Build the Mongo ULR for SqlAlchemy
username = "DataScience"
password = getpass("Password")
host = "coredb-rb0r4.mongodb.net"
database = "CoreDB"

conn_str = f"mongodb+srv://{username}:{password}@{host}/{database}"

# Configure the SqlAlchemy part of the app instance
app.config["MONGO_URI"] = conn_str

mongo = PyMongo(app)