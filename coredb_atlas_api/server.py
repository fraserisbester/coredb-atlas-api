
# TODO: Response timer

# External
from flask import render_template

# Internal
import config

# Get the application instance
connex_app = config.connex_app

# Read the swagger.json file to configure the endpoints
connex_app.add_api("swagger.yml")

# Create a URL route in our application for "/"
@connex_app.route("/")
def home():
    return render_template("home.html")

if __name__ == "__main__":
    connex_app.run(debug=True)
